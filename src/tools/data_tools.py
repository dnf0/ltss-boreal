import os
import re
import sys
import ssl
import urllib3
import shutil
from urllib.request import urlopen, Request, URLError, HTTPError
import csv
import subprocess
import io
import pandas as pd
import pyproj
import numpy as np
from skimage import exposure
from skimage import filters
import imageio

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())


def file_processed(mf, k):
    f = os.path.join(os.environ.get("HASTY_DEST"), mf.replace(".hdf", "." + k + ".png"))
    if os.path.exists(f):
        return True
    else:
        return False


def make_png(aod, fire_rows, fire_cols, fname, k):
    null_mask = aod < 0
    aod[null_mask] = 0

    aod /= np.max(np.abs(aod))
    # print(aod.min(), aod.max())

    # p2, p98 = np.percentile(aod, (1, 99))
    # aod  = exposure.rescale_intensity(aod, in_range=(p2, p98))

    aod = exposure.equalize_adapthist(aod, clip_limit=0.01)
    aod = filters.gaussian(aod, sigma=0.4)
    aod *= 255.0 / aod.max()
    print(np.min(aod), np.max(aod), np.mean(aod))

    png = np.zeros([aod.shape[0], aod.shape[1], 3], dtype=int)
    for i in range(3):
        png[:, :, i] = aod

    # set fires
    png[fire_rows, fire_cols, 0] = 255
    png[fire_rows, fire_cols, 1] = 0
    png[fire_rows, fire_cols, 2] = 0

    imageio.imwrite(os.path.join(os.environ.get("HASTY_DEST"), fname.replace(".hdf", "." + k + ".png")), png)


def add_index_to_fires(lat, lon, fire_df, date_stub):

    date_to_find = pd.to_datetime(date_stub.strip("A").strip("T"), format="%Y%j%H%M")

    # subset fires to only those in the image and with certain FRP
    fire_subset_df = subset_fires_to_image(lat, lon, fire_df, date_to_find)
    # build sensor grid indexes
    image_rows, image_cols = grid_indexes(lat)

    # locate fires in sensor coordinates
    fire_rows, fire_cols = locate_fire_in_image(fire_subset_df, lat, lon, image_rows, image_cols)

    return fire_rows, fire_cols


def subset_fires_to_image(lat, lon, fire_df, date_to_find):
    fire_subset = fire_df[fire_df.date == date_to_find.date()].copy()
    fire_subset = fire_subset[((fire_subset.latitude > np.min(lat)) &
                               (fire_subset.latitude < np.max(lat)) &
                               (fire_subset.longitude > np.min(lon)) &
                               (fire_subset.longitude < np.max(lon)))]
    return fire_subset


def grid_indexes(lat):

    rows = np.arange(lat.shape[0])
    cols = np.arange(lat.shape[1])
    cols, rows = np.meshgrid(cols, rows)
    return rows, cols


def locate_fire_in_image(fire_coords, lats, lons, rows, cols):

    fire_rows = []
    fire_cols = []

    for fire_lat, fire_lon in zip(fire_coords.latitude.values, fire_coords.longitude.values):

        try:

            mask = (lats > fire_lat - 0.05) & (lats < fire_lat + 0.05) & \
                   (lons > fire_lon - 0.05) & (lons < fire_lon + 0.05)
            sub_lats = lats[mask]
            sub_lons = lons[mask]
            sub_rows = rows[mask]
            sub_cols = cols[mask]

            # find exact loc using haversine distance
            if not np.max(mask):
                continue
            sub_index = np.argmin(haversine(fire_lon, fire_lat, sub_lons, sub_lats))

            # check if either fire row or column is too close to edge of image
            row = sub_rows[sub_index]
            col = sub_cols[sub_index]

            # append row and col for  exact location
            fire_rows.append(row)
            fire_cols.append(col)

        except Exception as e:
            print(e)
            continue

    return fire_rows, fire_cols


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    All args must be of equal length.

    """
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2

    c = 2 * np.arcsin(np.sqrt(a))
    km = 6367 * c
    return km


def read_maiac_aod(hdf_file):
    """

    :param hdf_file: an open MAIAC hdf file
    :return: a dictionary of all the aod images contained in the file and the lat and lon grids
    """

    # set up data dict
    dd = {}

    # Read global attribute.
    fattrs = hdf_file.attributes(full=1)
    # need to select the most appropriate layer in the product
    timestamps = fattrs['Orbit_time_stamp'][0].split(' ')
    timestamps = [t for t in timestamps if t != '']  # valid timestamps

    for i, timestamp in enumerate(timestamps):

        # extract time
        t = re.search("[0-9]{11}[A-Z]{1}", timestamp).group()

        # Read dataset.
        aod = hdf_file.select('Optical_Depth_055')[i, :, :] * 0.001  # aod scaling factor
        aod[aod < 0] = -999  # just get rid of the filled values for now

        dd[t] = aod

    ga = fattrs["StructMetadata.0"]
    gridmeta = ga[0]
    # Construct the grid.  The needed information is in a global attribute
    # called 'StructMetadata.0'.  Use regular expressions to tease out the
    # extents of the grid.
    ul_regex = re.compile(r'''UpperLeftPointMtrs=\(
                                      (?P<upper_left_x>[+-]?\d+\.\d+)
                                      ,
                                      (?P<upper_left_y>[+-]?\d+\.\d+)
                                      \)''', re.VERBOSE)
    match = ul_regex.search(gridmeta)
    x0 = np.float(match.group('upper_left_x'))
    y0 = np.float(match.group('upper_left_y'))

    lr_regex = re.compile(r'''LowerRightMtrs=\(
                                      (?P<lower_right_x>[+-]?\d+\.\d+)
                                      ,
                                      (?P<lower_right_y>[+-]?\d+\.\d+)
                                      \)''', re.VERBOSE)
    match = lr_regex.search(gridmeta)
    x1 = np.float(match.group('lower_right_x'))
    y1 = np.float(match.group('lower_right_y'))
    ny, nx = aod.shape
    xinc = (x1 - x0) / nx
    yinc = (y1 - y0) / ny

    x = np.linspace(x0, x0 + xinc * nx, nx)
    y = np.linspace(y0, y0 + yinc * ny, ny)
    xv, yv = np.meshgrid(x, y)

    # In basemap, the sinusoidal projection is global, so we won't use it.
    # Instead we'll convert the grid back to lat/lons.
    sinu = pyproj.Proj("+proj=sinu +R=6371007.181 +nadgrids=@null +wktext")
    wgs84 = pyproj.Proj("+init=EPSG:4326")
    lon, lat = pyproj.transform(sinu, wgs84, xv, yv)

    return dd, lat, lon


def load_fire_df():
    df_list = []
    for f in os.listdir(os.environ.get("FIRES_DEST")):
        df_list.append(pd.read_csv(os.path.join(os.environ.get("FIRES_DEST"), f)))
    fire_df = pd.concat(df_list)

    fire_df['date'] = pd.to_datetime(fire_df['acq_date']).dt.date
    fire_df = fire_df[['date', 'latitude', 'longitude']]
    return fire_df


def geturl(url, out=None):
    """
    Retrieves a target URL

    Args:
        url (str): The target URL to retrieve
        out (str): The directory to sync

    Returns:
        None

    """
    headers = {'user-agent': os.environ.get("LAADS_USERAGENT") + sys.version.replace('\n', '').replace('\r', '')}
    headers['Authorization'] = 'Bearer ' + os.environ.get("LAADS_TOKEN")

    try:
        CTX = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        if sys.version_info.major == 2:
            try:
                fh = urllib3.urlopen(urllib3.Request(url, headers=headers), context=CTX)
                if out is None:
                    return fh.read()
                else:
                    shutil.copyfileobj(fh, out)
            except urllib3.HTTPError as e:
                print('HTTP GET error code: %d' % e.code(), file=sys.stderr)
                print('HTTP GET error message: %s' % e.message, file=sys.stderr)
            except urllib3.URLError as e:
                print('Failed to make request: %s' % e.reason, file=sys.stderr)
            return None

        else:
            try:
                fh = urlopen(Request(url, headers=headers), context=CTX)
                if out is None:
                    return fh.read().decode('utf-8')
                else:
                    shutil.copyfileobj(fh, out)
            except HTTPError as e:
                print(e)
            except URLError as e:
                print('Failed to make request: %s' % e.reason, file=sys.stderr)
            return None

    except AttributeError:
        # OS X Python 2 and 3 don't support tlsv1.1+ therefore... curl
        try:
            args = ['curl', '--fail', '-sS', '-L', '--get', url]
            for (k, v) in headers.items():
                args.extend(['-H', ': '.join([k, v])])
            if out is None:
                # python3's subprocess.check_output returns stdout as a byte string
                result = subprocess.check_output(args)
                return result.decode('utf-8') if isinstance(result, bytes) else result
            else:
                print(" ".join(args))
                subprocess.call(args, stdout=out)
        except subprocess.CalledProcessError as e:
            print('curl GET error message: %' + (e.message if hasattr(e, 'message') else e.output), file=sys.stderr)
        return None


def sync_modis_dir(source, target_file_prefix, destination):
    """
    Function to retreive MODIS data if not already found in destination

    Args:
        source (str): HTTPS location
        target_file_prefix (str): MODIS prefix to be matched in the HTTPS location
        destination (str): Location to which file will be sent

    Returns:
        None

    """

    # Load all files in the src directory
    files = [f for f in csv.DictReader(io.StringIO(geturl('%s.csv' % source)), skipinitialspace=True)]

    # find matched file
    matched_file = [f for f in files if ".".join(f['name'].split(".")[:3]) == target_file_prefix]

    if matched_file:
        matched_file = matched_file[0]
        path = os.path.join(destination, matched_file['name'])
        url = source + '/' + matched_file['name']
        print(url)
        if not os.path.exists(path) or os.stat(path).st_size == 0:
            with open(path, 'w+b') as fh:
                geturl(url, fh)
    else:
        return None


def generate_modis_hv_fname_prefix(dt, h, v, tag="MCD19A2.A"):
    """
    Generate MODIS Sin Grid filename prefix

    Args:
        row (pd.Series): plume information
        h (int): h coordinate
        v (int): v coordinate
        tag (str): MODIS target product

    Returns:
        str: Formatted MODIS Sin Grid Filename

    """

    y = dt.year
    doy = str(dt.timetuple().tm_yday).zfill(3)
    h = str(h).zfill(2)
    v = str(v).zfill(2)
    return f"{tag}{y}{doy}.h{h}v{v}"


def retrieve_maiac(target_file_prefix):
    """
    Main function to get MODIS data

    Args:
        target_file_prefix (str): A MODIS filename prefix

    Returns:
        None

    """

    tag = target_file_prefix.split(".")[0]
    root = os.path.join("https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/6/", tag)

    # build source path
    ydoy = target_file_prefix.split(".")[1].strip('A')
    y, doy = ydoy[0:4], ydoy[4:].zfill(3)
    source = os.path.join(root, y, doy)

    # make destination directory if it doesn't exist
    outpath = os.path.join(os.environ.get("MAIAC_DEST"), y, doy)
    if not os.path.exists(outpath):
        os.makedirs(outpath)

    # retrieve target file
    sync_modis_dir(source, target_file_prefix, outpath)
