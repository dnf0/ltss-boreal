import os
import glob
from pyhdf.SD import SD, SDC


from src.tools import data_tools

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())


def main():
    fire_df = data_tools.load_fire_df()

    for file_path in glob.glob(os.environ.get("MAIAC_DEST") + '/**/*.hdf', recursive=True):

        try:
            hdf_file = SD(file_path, SDC.READ)
        except:
            continue
        aod_dict, lat, lon = data_tools.read_maiac_aod(hdf_file)
        for k in aod_dict.keys():

            # check if png already exists
            if data_tools.file_processed(file_path.split("/")[-1], k):
                print(file_path.split("/")[-1], "already processed")
                continue

            fire_rows, fire_cols = data_tools.add_index_to_fires(lat, lon, fire_df, k)
            if not fire_rows:
                continue

            data_tools.make_png(aod_dict[k], fire_rows, fire_cols, file_path.split("/")[-1], k)


if __name__ == "__main__":
    main()