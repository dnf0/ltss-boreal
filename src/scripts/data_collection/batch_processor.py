import datetime
from src.tools import data_tools


def retrieve(prefixes):
    for prefix in prefixes:
        data_tools.retrieve_maiac(prefix)


def main():

    # TODO implement as settings file
    min_h = 22
    max_h = 28
    min_v = 1
    max_v = 3
    year = 2020
    months = [6, 7, 8]
    days = [[23, 28, 30],
            [1, 2, 5, 15, 16, 23],
            [15, 16, 17, 18, 22, 23, 24, 25, 26, 27]]

    datetimes = []
    for m, month in enumerate(months):
        for day in days[m]:
            datetimes.append(datetime.datetime(year=year, month=month, day=day))

    prefixes = []
    for dt in datetimes:
        for h in range(min_h, max_h+1):
            for v in range(min_v, max_v+1):
                prefixes.append(data_tools.generate_modis_hv_fname_prefix(dt, h, v, tag="MCD19A2.A"))

    retrieve(prefixes)


if __name__ == "__main__":
    main()