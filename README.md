**Longterm Single Science Boreal Forest Fire Emissions**

This repo contains the code to estimate particulate matter emissions from boreal wildfire observations.  It uses a combintation of MODIS/VIIRS FRP data from the FIRMS archive to estimate FRE for high latitude fires through exploitation of orbital convergence (and the more regular overpass period this provides).  The FRE is then associated with TPM measures derived from MAIAC AOD for hundreds of hand digitsed plumes polygons generated using the Hast.ai digitsation tool.  


